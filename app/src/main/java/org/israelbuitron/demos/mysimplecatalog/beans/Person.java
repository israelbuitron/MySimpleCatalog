package org.israelbuitron.demos.mysimplecatalog.beans;

/**
 * Person class.
 * @author Israel Buitron
 */
public class Person {
    private int mId;
    private String mName;

    public Person() {
        // Empty constructor
    }

    public Person(int id, String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
