package org.israelbuitron.demos.mysimplecatalog.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.israelbuitron.demos.mysimplecatalog.R;
import org.israelbuitron.demos.mysimplecatalog.beans.Person;
import org.israelbuitron.demos.mysimplecatalog.dummy.DummyContent;

import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PersonDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonDetailFragment extends Fragment {

    // the fragment initialization parameters
    private static final String ARG_PERSON_ID = "paramPersonId";

    // Parameters supported in fragment
    private Integer mParamPersonId;

    // Views
    private TextView mPersonIdView;
    private TextView mPersonNameView;

    public PersonDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param personId Id from person object to display in fragment.
     * @return A new instance of fragment PersonDetailFragment.
     */
    public static PersonDetailFragment newInstance(Integer personId) {
        PersonDetailFragment fragment = new PersonDetailFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_PERSON_ID, personId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamPersonId = getArguments().getInt(ARG_PERSON_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_person_detail, container, false);

        mPersonIdView = (TextView) view.findViewById(R.id.fragment_person_detail_id);
        mPersonNameView = (TextView) view.findViewById(R.id.fragment_person_detail_name);

        // Get datasource
        Map<Integer,Person> dataSource = DummyContent.PEOPLE_MAP;
        Person p = dataSource.get(mParamPersonId);

        // Fill views with data
        mPersonIdView.setText(Integer.toString(p.getId()));
        mPersonNameView.setText(p.getName());;

        return view;
    }

}
