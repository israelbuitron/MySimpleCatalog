package org.israelbuitron.demos.mysimplecatalog.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.israelbuitron.demos.mysimplecatalog.R;
import org.israelbuitron.demos.mysimplecatalog.beans.Person;
import org.israelbuitron.demos.mysimplecatalog.fragments.PersonDetailFragment;
import org.israelbuitron.demos.mysimplecatalog.fragments.PersonFragment;

/**
 * @author Israel Buitron
 */
public class MainActivity extends AppCompatActivity
        implements PersonFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onListFragmentInteraction(Person item) {
        // Fragment
        PersonDetailFragment pdf = PersonDetailFragment.newInstance(item.getId());

        // Change fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, pdf)
                //.replace(R.id.frag, pdf)
                .addToBackStack(null)
                .commit();
    }
}
