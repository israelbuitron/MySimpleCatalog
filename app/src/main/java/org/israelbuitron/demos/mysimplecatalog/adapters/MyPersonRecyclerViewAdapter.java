package org.israelbuitron.demos.mysimplecatalog.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.israelbuitron.demos.mysimplecatalog.R;
import org.israelbuitron.demos.mysimplecatalog.beans.Person;
import org.israelbuitron.demos.mysimplecatalog.fragments.PersonFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Person} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class MyPersonRecyclerViewAdapter extends RecyclerView.Adapter<MyPersonRecyclerViewAdapter.ViewHolder> {

    private final List<Person> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyPersonRecyclerViewAdapter(List<Person> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate layout for a single item in list
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.fragment_person, parent, false);

        // Create holder for each view (an item in list)
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setPerson(mValues.get(position));

        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.getPerson());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // {@link fragment_person} layout
        public final View mView;

        // Person id textview
        public final TextView mPersonIdView;

        // Person name textview
        public final TextView mPersonNameView;

        // Person
        public Person mPerson;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPersonIdView = (TextView) view.findViewById(R.id.fragment_person_id);
            mPersonNameView = (TextView) view.findViewById(R.id.fragment_person_name);
        }

        public Person getPerson() {
            return mPerson;
        }

        public void setPerson(Person person) {
            mPerson = person;
            mPersonIdView.setText(Integer.toString(mPerson.getId()));
            mPersonNameView.setText(mPerson.getName());
        }

        public View getView() {
            return mView;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPersonNameView.getText() + "'";
        }
    }
}
